Quiero tener estas diapositivas en mi tema de presentación:

-   [✓] Imágen en el fondo

-   [ ] Imágen en el fondo con texto

-   [ ] Fondo con texto gigante monotipo

-   [ ] Barra inferior con marca

Todas las diapositivas deberían tener espacio para citas encima de la cinta inferior.
