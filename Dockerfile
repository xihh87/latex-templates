FROM pandoc/latex:2.12

RUN apk add --no-cache \
	--repository=http://dl-cdn.alpinelinux.org/alpine/edge/testing/ \
	9base

ENV PATH /opt/texlive/texdir/bin/x86_64-linuxmusl:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/lib/9base/bin
