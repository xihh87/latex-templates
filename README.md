<img src="logo.png" height="64px"/>

[más badges]

Funciones y plantillas de LaTeX para mis documentos.

Instrucciones de instalación

```
$ make install
```

Instrucciones de uso

```
$ mklatex
```

## Casos de uso

## Cómo contribuir

1.  Revisar los [issues abiertos](https://gitlab.com/xihh87/latex-templates/-/issues ).

2.  Proponer un cambio en la plantilla.

3.  Verificar que compila usando la imagen de docker.
