-   [ ] [Insertar animaciones en mis presentaciones pdf](https://stackoverflow.com/questions/9009771/is-it-possible-to-embed-animated-gifs-in-pdfs#14852633)

    [Conviene optimizar los videos antes con Kazaam](https://tex.stackexchange.com/questions/429/animation-in-pdf-presentations-without-adobe-reader)

-   [ ] Las animaciones pueden verse usando:

    -    [Adobe Reader (wine)](https://www.linuxquestions.org/questions/linux-software-2/pdf-with-animations-4175537446/)
    -    [`impress!ive`](http://impressive.sourceforge.net/)
    -    [`pdfpc`](https://pdfpc.github.io/) soporta multimonitor, notas y videos.
